# SYSLOG to Palo Alto User ID Engine


## Contributors : 

- NOMIOS Technical Team
- TiTom73 (titom@inetsix.net)

## DESCRIPTION

Monitor any file to parse log entries and identify user login / logout and send userid information to Palo Alto user id engine.

### Usage :

Configuration file required to run script 
```
[general]
; Palo Alto User ID Engine
userid_engine = 10.73.1.1
; User Domain used to send to PA
user_domain = NMS
; File to monitor
monitor_file = /path/to/syslog/file.log
; Regexp Dictionnary
regexp_file = /path/to/regexp.dict
; Enable / Disable DEBUG Status
debug = 1
```
Then, run script :
```
user@station>perl syslog-to-PA-userid.pl 
```

## THANKS
Palo Alto to provide API based on Perl
Nomios technical team for all tests

## CHANGELOG
- 2013-07-11 : v0.2 Add configuration file support
- 2013-07-01 : v0.11 Add Regexp dictionnary
- 2013-06-30 : First version with static parameters

## INSTALL
To run, we must install required perl library :
- File::Tail
- Switch
- Config::IniFiles
- IO::SOCKET::SSL

Library provided with this script : PAN::API
Be advise we have to modify some lines in PAN:API so please do not use directly lib available on https://www.paloaltonetworks.com/

Next, you have to enter required data in configuration file (config.cfg) to provide informations.


## LICENSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

